# Liste des outils autour du développement

### Editeurs de texte
* Atom
* Notepad++
* Stackedit: Editeur de markdown en ligne [(Lien)](https://stackedit.io/app#)

### XML
* XMLLInt : Validation de schéma XML  [(Lien)](http://xmlsoft.org/xmllint.html)

```
xmllint --schema /schema_path.xsd /path_to_xml_file.xml --noout
```

### Images libres

* [Pexels](https://www.pexels.com) 

